Symblog2
========================

Symblog2 is influenced by the famous Symblog blogging application powered by Symfony2. It uses the latest twitter bootstrap 3.x front-end UI framework and comes with more advanced UI and responsive layout along with many other feature improvements.  The codebase is 100% PSR2 compatible and it uses some most popular doctrine extensions along with many useful bundle.  

1) Installing Symblog2
----------------------------------

Install git on your system and fork the project. After cloning is done, run `composer update` to install all the necessary bundle.  

Enjoy!

